import {Component} from 'react'


class Music extends Component{
  render(){
    return(
      <>
      <div className="navigation__list">
          <div
            className="navigation__list__header"
            role="button"
            data-toggle="collapse"
            href="#yourMusic"
            aria-expanded="true"
            aria-controls="yourMusic"
          >
            Your Music
          </div>
          <div className="collapse in" id="yourMusic">
            <a href="#" className="navigation__list__item">
              <i className="ion-headphone" />
              <span>Songs</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-ios-musical-notes" />
              <span>Albums</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-person" />
              <span>Artists</span>
            </a>
            <a href="#" className="navigation__list__item">
              <i className="ion-document" />
              <span>Local Files</span>
            </a>
          </div>
        </div>
      </>
    )
  }
}

export default Music