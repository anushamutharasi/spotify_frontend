import { Component } from "react";

class ArtistNavigation extends Component{
    render(){
        return(
            <div className="artist__navigation">
            <ul className="nav nav-tabs" role="tablist">
              <li role="presentation" className="active">
                <a
                  href="#artist-overview"
                  aria-controls="artist-overview"
                  role="tab"
                  data-toggle="tab"
                >
                  Overview
                </a>
              </li>
              <li role="presentation">
                <a
                  href="#related-artists"
                  aria-controls="related-artists"
                  role="tab"
                  data-toggle="tab"
                >
                  Related Artists
                </a>
              </li>
            </ul>
          </div>
        )
    }
}

export default ArtistNavigation