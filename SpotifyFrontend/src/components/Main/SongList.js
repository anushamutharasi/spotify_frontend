import { Component } from "react";
import Song from "./Song";

class SongList extends Component{
    render(){
        const{song_info} = this.props.song_info
        return(
            <div className="album__tracks">
                <div className="tracks">
                    <div className="tracks__heading">
                    <div className="tracks__heading__number">#</div>
                    <div className="tracks__heading__title">Song</div>
                    <div className="tracks__heading__length">
                        <i className="ion-ios-stopwatch-outline" />
                    </div>
                    <div className="tracks__heading__popularity">
                        <i className="ion-thumbsup" />
                    </div>
                    </div>
                    {song_info.map(each=>(
                        <Song each={each} key={each.id}/>
                    ))}
                </div>
            </div>
        )
    }
}
export default SongList