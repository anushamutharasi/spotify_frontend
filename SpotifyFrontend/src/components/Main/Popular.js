import { Component } from "react";

import Tracks from "./Tracks";

class Popular extends Component {
  render() {
    const  {popular}  = this.props.popularlist;
    return (
      <>
        <div className="section-title">Popular</div>
        <div className="tracks">
          {popular.map((eachtrack) => (
            <Tracks tracks={eachtrack} key={eachtrack.number} />
          ))}
        </div>

        <button className="show-more button-light">Show 5 More</button>
      </>
    );
  }
}

export default Popular;
