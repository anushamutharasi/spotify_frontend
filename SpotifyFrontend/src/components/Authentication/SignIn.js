import { Component } from "react";

class SignIn extends Component{
  redirectToSignup = ()=>{
    const{history} = this.props
    history.replace("/signup")
  }
    render(){
        return(
            <div>
        <header className="top-header">
        </header>
        <div id="mainCoantiner">
          <div className="main-header">
          <img className='img2' src="https://www.pngkey.com/png/full/190-1907978_spotify-logo-png-white-spotify-logo-white-transparent.png" id="logo"/>
            <div className="folio-btn">
              <a className="folio-btn-item ajax" href="https://bit.ly/2y6huFG" target="_blank"><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /></a>
            </div>
          </div>
          {/*dust particel*/}
          <div>
            <div className="starsec" />
            <div className="starthird" />
            <div className="starfourth" />
            <div className="starfifth" />
          </div>
          {/*Dust particle end-*/}
          <div className="container text-center text-dark mt-5">
            <div className="row">
              <div className="col-lg-4 d-block mx-auto mt-5">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-md-12">
                    <div className="card">
                      <div className="card-body wow-bg" id="formBg">
                        <h3 className="colorboard">Login</h3>
                        <p className="text-muted">Sign In to your account</p>
                        <div className="input-group mb-3"> <input type="text" className="form-control textbox-dg" placeholder="Username" /> </div>
                        <div className="input-group mb-4"> <input type="password" className="form-control textbox-dg" placeholder="Password" /> </div>
                        <div className="row">
                          <div className="col-12"> <button type="button" className="btn btn-primary btn-block logn-btn">Login</button> </div>
                          <div className="col-12"> <a href="forgot-password.html" className="btn btn-link box-shadow-0 px-0">Forgot password?</a> </div>
                        </div>
                        <p className="additional-act">Don't have account? <span onClick={this.redirectToSignup}> Sign up </span></p>
                        <div className="mt-6 btn-list">
                          <button type="button" className="socila-btn btn btn-icon btn-facebook fb-color"><a href="https://www.facebook.com/" target="_blank"><i className="fab fa-facebook-f faa-ring animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-google incolor"><a href="https://www.linkedin.com/" target="_blank"><i className="fab fa-linkedin-in faa-flash animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-twitter tweetcolor"><a href="https://www.twitter.com/" target="_blank"><i className="fab fa-twitter faa-shake animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-dribbble driblecolor"><a href="https://www.google.com/" target="_blank"><i className="fab fa-dribbble faa-pulse animated" /></a></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        )
    }
}
export default SignIn