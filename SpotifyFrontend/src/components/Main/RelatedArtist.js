import { Component } from "react";

class RelatedArtist extends Component{
    render(){
        const{each} = this.props
        return(
            <div className="media-card">
                <div
                    className="media-card__image"
                    style={{
                      backgroundImage:
                        `url(${each.image})`
                    }}
                >
                <i className="ion-ios-play" />
                </div>
                <a className="media-card__footer">{each.name}</a>
            </div>
        )
    }
}

export default RelatedArtist