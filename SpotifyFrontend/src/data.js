const url = "http://localhost:3000"
const header={
    profile_image:`${url}/images/header/adam_proPic.jpg`,
    name:" Adam Lowenthal"

}

const main_panel={
    artist:{
        name:"G-Eazy",
        artist_image:`${url}/Images/main/Artist/g_eazy_propic.jpg`,
        monthly_listeners:15662810
        },
     latest_image:{
         latest_release_image:`${url}/Images/latest_release/whenDarkOut.jpg`,
         date:"4 December 2015",
         title:"Drifiting (Track Commentary)"

     },
      
}
const popular=[
        {
        image:`${url}/Images/latest_release/whenDarkOut.jpg`,
        song_name:"Me,Myself&I",
        listner_count:1447544165,
        },
        {
            image:`${url}/Images/latest_release/tth.jpg`,
            song_name:"I Mean It",
            listner_count:74568782,
            },
        {
            image:`${url}/Images/latest_release/whenDarkOut.jpg`,
            song_name:"Calm Calm Down",
            listner_count:13737506,
            
        },
        {
            image:`${url}/Images/latest_release/whenDarkOut.jpg`,
            song_name:"Some Kind Of Drug",
            listner_count:12234881,
        },{
            image:`${url}/Images/latest_release/tth.jpg`,
            song_name:"Let's Get Lost ",
            listner_count:40882954
            

        }


]
const related_artist=[
    {
        id:1,
        image:`${url}/Images/related_artist/hoodie.jpg`,
        name:"Hoodie Allen"
        
    },
    {
        id:2,
        image:`${url}/Images/related_artist/drake.jpeg`,
        name:"Drake"
        
    },
    {
        id:3,
        image:`${url}/Images/related_artist/bigsean.jpg`,
        name:"Big Sean"
        
    },
    {
        id:4,
        image:`${url}/Images/related_artist/yonas.jpg`,
        name:"Yonas"
        
    },
    {
        id:5,
        image:`${url}/Images/related_artist/mikestud.jpg`,
        name:"Mike Stud"
        
    },
    {
        id:6,
        image:`${url}/Images/related_artist/jcole.jpg`,
        name:"J. Cole"
        
    },
    {
        id:7,
        image:`${url}/Images/related_artist/wiz.jpeg`,
        name:"Wiz Khalifa"
        
    },
]
const albums={
    
        image:`${url}/Images/latest_release/whenDarkOut.jpg`,
        year:2015,
        name:"When It's Dark Out"
}
const  song_info=[
    {
        id:1,
        song_name:"Intro",
        song_time:"1:11",
        is_check:true

    },
    {
        id:2,
        song_name:"Random",
        song_time:"3:00",
        is_check:true

    },
    {
        id:3,
        song_name:"Me, Myself & I- Bebe Rexha ",
        song_time:"4:11",
        is_check:true


    },
    {
        id:4,
        song_name:"One Of Them- Big Sean",
        song_time:"3:20",
        is_check:false

    },
    {
        id:5,
        song_name:"Drifting - Chris Brown ,Tory Lanez",
        song_time:"4:33",
        is_check:false

    },
    {
        id:6,
        song_name:"Of All Things Too $hort",
        song_time:"3:34",
        is_check:false

    },
    {
        id:7,
        song_name:"Order More Starrah",
        song_time:"3:29",
        is_check:true

    },
    {
        id:8,
        song_name:"Calm Down ",
        song_time:"2:07",
        is_check:true

    },
    {
        id:9,
        song_name:"Don't Let Me Go Grace ",
        song_time:"3:11",
        is_check:false

    },
    {
        id:10,
        song_name:"You Got Me",
        song_time:"3:28",
        is_check:true

    },
    {
        id:11,
        song_name:"What If - Gizzie",
        song_time:"4:13",
        is_check:true

    },
    {
        id:12,
        song_name:"Sad Boy",
        song_time:"3:23",
        is_check:true

    },
    {
        id:13,
        song_name:"Some Kind Of Drug Marc E. Bassy",
        song_time:"3:42",
        is_check:false

    },
    {
        id:14,
        song_name:"Think About You Quin ",
        song_time:"2:59",
        is_check:false

    },
    {
        id:15,
        song_name:"Everything Will Be OK Kehlani",
        song_time:"5:11",
        is_check:true

    },
    

    {
        id:16,
        song_name:"For This Iamnobodi",
        song_time:"4:11",
        is_check:false

    },
    

    {
        id:17,
        song_name:"Nothing to Me Keyshia Cole E-40",
        song_time:"5:30",
        is_check:true

    },
]


export default {popular,header,main_panel,song_info,albums,related_artist}